package com.binary_studio.tree_max_depth;

import com.google.common.graph.ImmutableNetwork;

import java.util.*;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		Integer depth = 0;

		if (rootDepartment==null) return 0;

		Stack<Department>departments=new Stack<>();
		departments.push(rootDepartment);
		while (true) {
			int stackSize = departments.size();
			System.out.println("stack size: "+stackSize);
			if (stackSize==0) return depth;
			depth++;

			while (stackSize>0) {
				System.out.println("size before deleting: "+departments.size());
				List<Department> subNodesOfDeletedNode=departments.pop().subDepartments
															.stream()
															.filter(Objects::nonNull)
															.collect(Collectors.toList());
				departments.trimToSize();
				System.out.println("size after deleting: "+departments.size());
				subNodesOfDeletedNode.stream().forEach(subNode->departments.push(subNode));
				stackSize--;
			}
		}
		}

}
