package com.binary_studio.uniq_in_sorted_stream;

import com.google.common.base.Predicate;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}


	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {

		return Optional.of(stream
								.filter(Objects::nonNull)
									.distinct()
										.sorted(Comparator.comparing(Row::getPrimaryId)))
											.orElseGet(Stream::empty);

	}

	public static void main(String[] args) {
		Stream<Row<Object>>stream=uniqueRowsSortedByPK(Stream.of(1,2,3,7,4,null,3).filter(r->r!=null).map(Integer::toUnsignedLong).map(Row::new));
		stream.forEach(Object::toString);
	}
}
