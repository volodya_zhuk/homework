package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {


	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private DockedShipBuilder dockedShipBuilder;

	public DockedShip(DockedShipBuilder dockedShipBuilder) {
		this.dockedShipBuilder = dockedShipBuilder;
	}


	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
									   PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
									   PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(DockedShipBuilder
				.named(name)
				.hull(hullHP.value())
				.shield(shieldHP.value())
				.pg(powergridOutput.value())
				.capacitor(capacitorAmount.value())
				.capacitorRegen(capacitorRechargeRate.value())
				.speed(speed.value())
				.size(size.value()));
	}

	public DockedShipBuilder getDockedShipBuilder() {
		return dockedShipBuilder;
	}

	public AttackSubsystem getAttackSubsystem() {
		return attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return defenciveSubsystem;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {

		if(subsystem==null){this.attackSubsystem=null;return;}

		if( dockedShipBuilder.getPg().value()<subsystem.getPowerGridConsumption().value()) {
			throw new InsufficientPowergridException(subsystem.getPowerGridConsumption().value() -
					dockedShipBuilder.getPg().value());
		}
		else
			dockedShipBuilder.pg(dockedShipBuilder.getPg().value()-subsystem.getPowerGridConsumption().value());
			this.attackSubsystem=subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if(subsystem==null ) {this.defenciveSubsystem=null;return;}

		if( dockedShipBuilder.getPg().value()<subsystem.getPowerGridConsumption().value()) {
			throw new InsufficientPowergridException(subsystem.getPowerGridConsumption().value() -
					dockedShipBuilder.getPg().value());
		}
		else
			dockedShipBuilder.pg(dockedShipBuilder.getPg().value()-subsystem.getPowerGridConsumption().value());
			this.defenciveSubsystem=subsystem;
	}


	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if(defenciveSubsystem ==null & attackSubsystem==null) throw  NotAllSubsystemsFitted.bothMissing();
		if(defenciveSubsystem==null & attackSubsystem!=null)throw NotAllSubsystemsFitted.defenciveMissing();

		if (defenciveSubsystem!=null & attackSubsystem==null)throw NotAllSubsystemsFitted.attackMissing();

		return new CombatReadyShip(this);
	}



}
