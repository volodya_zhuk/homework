package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

import java.util.Optional;

public final class CombatReadyShip implements CombatReadyVessel {

	private PositiveInteger startCapacity;
	private static   PositiveInteger startShieldHP;
	private static PositiveInteger startHullHP;
	private DockedShip ship;

	public CombatReadyShip(DockedShip ship) {
		this.ship = ship;
	}

	@Override
	public void endTurn() {
		System.out.println("ending out turn");
			ship.getDockedShipBuilder().capacitor(ship.getDockedShipBuilder().getCapacitorRegeneration().value()
				+ship.getDockedShipBuilder().getCapacitor().value());
	}

	@Override
	public void startTurn() {
		startCapacity=ship.getDockedShipBuilder().getCapacitor();
		startHullHP=ship.getDockedShipBuilder().getHullHP();
		startShieldHP=ship.getDockedShipBuilder().getShieldHP();
		System.out.println("starting our turn");
	}

	@Override
	public String getName() {
		return ship.getDockedShipBuilder().getName();
	}

	@Override
	public PositiveInteger getSize() {
		return ship.getDockedShipBuilder().getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return ship.getDockedShipBuilder().getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		var damage=ship.getAttackSubsystem().attack(target);
		var attacker =(NamedEntity)ship.getDockedShipBuilder();
		var weapon=ship.getAttackSubsystem();
		if(ship.getDockedShipBuilder().getCapacitor().value()<ship.getAttackSubsystem().getCapacitorConsumption().value())
		{
			return Optional.empty();
		}else
			ship.getDockedShipBuilder().capacitor(ship.getDockedShipBuilder().getCapacitor().value()
												-ship.getAttackSubsystem().getCapacitorConsumption().value());
		return Optional.of(new AttackAction(damage,attacker,target,weapon));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		var damage=ship.getDefenciveSubsystem().reduceDamage(attack).getDamage();
		if(damage.value()>ship.getDockedShipBuilder().getShieldHP().value())
		{
			var damageOnVessel= damage.value()-ship.getDockedShipBuilder().getShieldHP().value();
			ship.getDockedShipBuilder().hull(ship.getDockedShipBuilder().getHullHP().value()-damageOnVessel);
			ship.getDockedShipBuilder().shield(0);
		}else{
			ship.getDockedShipBuilder().shield(ship.getDockedShipBuilder().getShieldHP().value()-damage.value());
		}
		if(ship.getDockedShipBuilder().getHullHP().value()<=0)
		{
			return new AttackResult.Destroyed();
		}
		return new AttackResult.DamageRecived(attack.weapon,damage,attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {

		if(ship.getDockedShipBuilder().getShieldHP().value()>=startShieldHP.value() && ship.getDockedShipBuilder().getHullHP()==startHullHP)
		{
			return Optional.of(new RegenerateAction(PositiveInteger.of(0),PositiveInteger.of(0)));
		}
		else
		{
			return Optional.of(ship.getDefenciveSubsystem().regenerate());
		}
	}

}
