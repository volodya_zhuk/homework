package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

import java.util.Optional;

public final class AttackSubsystemImpl implements AttackSubsystem, NamedEntity {

    private  AttackSubsystemBuilder attackSubsystemBuilder;

	public AttackSubsystemImpl(AttackSubsystemBuilder attackSubsystemBuilder) {
        this.attackSubsystemBuilder=attackSubsystemBuilder;
    }

    public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
                                                PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
                                                PositiveInteger baseDamage) throws IllegalArgumentException {
	    if(name == null || name.trim().isEmpty())throw new IllegalArgumentException("Name should be not null and not empty");
	    	    return new AttackSubsystemImpl(AttackSubsystemBuilder.named(name)
                .pg(powergridRequirments.value())
                .capacitorUsage(capacitorConsumption.value())
                .optimalSpeed(optimalSpeed.value())
                .optimalSize(optimalSize.value())
                .damage(baseDamage.value()));
    }


	@Override
	public PositiveInteger getPowerGridConsumption() {
		return attackSubsystemBuilder.getPgRequirement();
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return attackSubsystemBuilder.getCapacitorUsage();
	}

	@Override
	public PositiveInteger attack(Attackable target) {
        var targetSize=(double)target.getSize().value();
        var optimalSize=(double)attackSubsystemBuilder.getOptimalSize().value();

        var sizeReductionModifier =
	            targetSize>=optimalSize?1:(targetSize/optimalSize);

	    var targetSpeed=(double)target.getCurrentSpeed().value();
	    var optimalSpeed=(double)attackSubsystemBuilder.getOptimalSpeed().value();

	    var speedReductionModifier=
	      targetSpeed<=optimalSpeed?1:(optimalSpeed/(2*targetSpeed));

	    var baseDamage=(double)attackSubsystemBuilder.getBaseDamage().value();

	    return PositiveInteger.of((int)Math.ceil(baseDamage*(Math.min(sizeReductionModifier,speedReductionModifier))));
    }

	@Override
	public String getName() {
		return attackSubsystemBuilder.getName();
	}

}
