package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

import java.util.Optional;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {



	private DefenciveSubsystemBuilder defenciveSubsystemBuilder;

	public DefenciveSubsystemImpl(DefenciveSubsystemBuilder defenciveSubsystemBuilder)
	{
		this.defenciveSubsystemBuilder=defenciveSubsystemBuilder;
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if(name==null || name.trim().isEmpty())throw new IllegalArgumentException("Name should be not null and not empty");
		return new DefenciveSubsystemImpl(DefenciveSubsystemBuilder
				.named(name)
				.pg(powergridConsumption.value())
				.capacitorUsage(capacitorConsumption.value())
				.impactReduction(impactReductionPercent.value())
				.shieldRegen(shieldRegeneration.value())
				.hullRegen(hullRegeneration.value()));
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return defenciveSubsystemBuilder.getPgRequirement();
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return 	defenciveSubsystemBuilder.getCapacitorUsage();
	}

	@Override
	public String getName() {
		return defenciveSubsystemBuilder.getName();
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {

		int damage;
		double percent= 1- defenciveSubsystemBuilder.getImpactReduction().value()*0.01;
		if(defenciveSubsystemBuilder.getImpactReduction().value()>95)
		{
			damage=(int)Math.ceil(0.05*incomingDamage.damage.value());
		}else {
			damage=(int)Math.ceil(percent*incomingDamage.damage.value());
		}
		return new AttackAction(PositiveInteger.of(damage),incomingDamage.attacker,incomingDamage.target,incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(defenciveSubsystemBuilder.getShieldRegen(),defenciveSubsystemBuilder.getHullRegen());
	}

}
